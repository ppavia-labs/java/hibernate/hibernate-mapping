package ppa.labs.hibernate.hibernatemapping;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.Test;

import ppa.labs.hibernate.hibernatemapping.bean.Address;
import ppa.labs.hibernate.hibernatemapping.bean.Customer;
import ppa.labs.hibernate.hibernatemapping.bean.Location;

public class HibernateMappingMainTest {
	
	@Test
	void connexionTest () {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("HibernateMappingDB");
		EntityManager entityManager = factory.createEntityManager();
		 
		entityManager.getTransaction().begin();
		
		List<Address> addresses = new ArrayList<Address>();
		
		Location loc1 = new Location();
		loc1.setLatitude(new BigDecimal(0));
		loc1.setLongitude(new BigDecimal(0));
		loc1.setName("NORTH");
		loc1.setZipCode("000000");
		
		Address addr1 = new Address();
		addr1.setLocation(loc1);
		addr1.setNum("1");
		addr1.setStreetName("the pole");
		addresses.add(addr1);
		
		Customer customer1 = new Customer();
		customer1.setAddresses(addresses);
		customer1.setAge(47);
		customer1.setBirthday(LocalDate.of(1985, 5, 12));
		customer1.setFirstName("Andrew");
		customer1.setLastName("DO");
		
		entityManager.persist(customer1);
		
		entityManager.getTransaction().commit();
		entityManager.close();
		factory.close();
		
		assertEquals(2, 1+1);
	}
}
